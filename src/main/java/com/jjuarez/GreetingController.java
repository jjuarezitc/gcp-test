package com.jjuarez;

import java.net.InetAddress;
import java.net.UnknownHostException;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GreetingController {
	@RequestMapping("/greeting")
	public String greeting() {
		StringBuilder message = new StringBuilder("Hello papi!");
		try {
			InetAddress ip = InetAddress.getLocalHost();
			message.append(" From host: " + ip);
		} catch(UnknownHostException e) {
			e.printStackTrace();
		}
		return message.toString();
	}
}
